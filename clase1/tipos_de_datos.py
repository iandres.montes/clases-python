# String = str
string1 = 'Hola mundo x1'
string2 = " Hola mundo x2"
string3 = ''' Hola 
mundo x3'''

# print(type(string3))
# print(string2)
# print(string3)

# ================================================= # =============================

# Integer = int
integer1 = 123123
# print(type(integer1))

# ================================================= # =============================


# Arrays = list
arrays1 = ["Hola mundo", 123]
# print(type(arrays1))
# print( arrays1[1] )

# ================================================= # =============================

# Tuplas o listas = tuple
tl = ('numero', 123)
# print(type(tl))
# print(tl)

# ================================================= # =============================

# dictado o diccionario = dict
diccionario = { "español":"HOLA MUNDO", "ingles":"Hi" }
# print(type(diccionario))

# ================================================= # =============================

# Boolean = bool
boleano1 = True or False
boleano2 = 1 or 0
boleano3 = arrays1 or []
boleano4 = 'qwe' or ''

# ================================================= # =============================#

# Float = float
flotante = 123.123
# print(type(flotante))

# ==============================================# 

var1 = 12345
var1 = "hola"
print(type(var1))


#integer (int)
var1 = 789 #numero entero
var2 = 0 #cero
var3 = -192 #numero entero negativo
var4 = 0b010 
var5 = 0o642 # octal
var6 = 0xF3 # hexa

# float 
var1 = 9.23 
var2 = 0.0
var3 = -1.7e-6 # x10e-6

#Bool 
var1 = True
var2 = False

# String (str)
var1 = "One\nTwo" # String con salto de linea \n
var2 = 'I\'m' #string omitiendo una comilla con \
var3 = """X\tY\tZ
        1\t2\t3"""  #String con tabulaciones 
                    #en lugar de espacio con \t


#bytes
var1 = b"toto\xfe\775" #un byte compuesto 
                       #por un hexadecimal y un octal