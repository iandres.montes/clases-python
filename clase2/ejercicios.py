# Tipos de datos simples

# 1. Escribir un programa que muestre por pantalla la cadena ¡Hola Mundo!.

#print("¡Hola Mundo!")

# ============================== # ========================================#

# 2. Escribir un programa que almacene la cadena ¡Hola Mundo! en una variable y luego muestre por pantalla el contenido de la variable.

# var = "¡Hola Mundo!"
# print(var)

# ============================== # ========================================#

# 3. Escribir un programa que pregunte el nombre del usuario en la consola y después de que el usuario lo introduzca muestre por pantalla la cadena ¡Hola <nombre>!, donde <nombre> es el nombre que el usuario haya introducido.

# name = input("¿Cual es tu nombre? > ")
# edad = input("¿Cual es tu edad? > ")
# print("Tu edad es: {edad} y tu nombre es: {nombre} ".format(nombre=name,edad=edad))

# ============================== # ========================================#

# 4. Escribir un programa que muestre por pantalla el resultado de la siguiente operación aritmética 

# var = ((3+2)/(2-5))**2
# print(var)

# ============================ # =========================================#

# 5. Escribir un programa que pregunte al usuario por el número de horas trabajadas y el coste por hora. Después debe mostrar por pantalla la paga que le corresponde.

# try:
#     horas = int(input("Horas trabajadas: "))
#     coste = int(input("Coste por hora: "))

#     total = horas * coste
#     print(f"El valor a pagar es: {total}")
# except:
#     print("Por favor, escriba un numero")
# finally:
#     print("Finalizado!")

# ============================ # =========================================#

# 6. Escribir un programa que lea un entero positivo, n , introducido por el usuario y después muestre en pantalla la suma de todos los enteros desde 1 hasta n . La suma de los n primeros enteros positivos puede ser calculada de la siguiente forma:

# n = int(input("Ingresa el numero: "))
# var = n * (n + 1) / 2
# print(var)

# ============================ # =========================================#

# 7. Escribir un programa que pida al usuario su peso (en kg) y estatura (en metros), calcule el índice de masa corporal y lo almacene en una variable, y muestre por pantalla la frase Tu índice de masa corporal es <imc> donde <imc> es el índice de masa corporal calculado redondeado con dos decimales.

# peso = float(input("¿Cual es tu peso? > "))
# estatura = float(input("¿Cual es tu estatura? > "))

# imc = "{:.2f}".format(peso*estatura)

# print(f"Tu índice de masa corporal es: {imc}")

# ============================ # =========================================#
